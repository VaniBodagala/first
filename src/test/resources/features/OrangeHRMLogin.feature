#Author: vani
#Feature: List of scenarios.
#Scenario: POM example
Feature: feature to verify OrangeHRM login with valid credentials

 
  @First
   Scenario: verify OrangeHRM admin login functionality
    Given user is on OrangeHRM Login page
    When Login page is verified
    Then user enters username and password and click on Login button
    Then user is navigated to dashboard page
    
 ##Madhavi's code   
   @First
Scenario: verify OrangeHRM logo
Given User opens browser 
Then title should be OrangeHRM
Then verify to see if OrangeHRMlogo is displayed or not


#login with admin role
@First
Scenario: Verify OrangeHRM login and logout functionality
Given User should open browser 
Then username should be Admin and password should be admin123 and click on Login button
Then user will be on Dashboard page
And click on logout link
Then verify footer text of OrangeHRm page 

# testing linkedin icon 
@First
Scenario: OrangehRM social media links linkedin
Given User should be loginpage 
Then check linkedin icon for hrmpage

# testing Fbook icon 
@First
Scenario: OrangehRM social media link Facebook
Given User should be loginpagehrm 
Then check Fbook icon for hrmpage

 #testing twitter icon 
@First
Scenario: OrangehRM social media link twitter
Given User has to be loginpage
Then test twitter link foor hrm page

# testing youtube icon 
@First
Scenario: OrangehRM social media link youtube
Given User must be loginpage
Then test youtube link for hrm page
