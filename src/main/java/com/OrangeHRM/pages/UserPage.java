package com.OrangeHRM.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;

import com.OrangeHRM.util.BasePage;

public class UserPage extends BasePage{

	private By btn_addUser=By.id("btnAdd");
	private By userList=By.xpath("//table[@id='resultTable']//td/a");
	private By btn_deleteUser=By.id("btnDelete");
	private By btn_deleteOk=By.id("dialogDeleteBtn");
	private By chk_user=By.xpath("parent::td/preceding-sibling::td/input");
	
	public WebElement getAddUserBtn()
	{
		return getElement(btn_addUser);
	}
	public List<WebElement> getUsers()
	{
		return getElements(userList);
	}
	public WebElement getDeleteBtn()
	{
		return getElement(btn_deleteUser);
	}
	public WebElement getDeleteOkBtn()
	{
		return getElement(btn_deleteOk);
	}
	public WebElement getUserCheckBox(WebElement user)
	{
		return user.findElement(chk_user);
	}
	
	public WebElement getUser(List<WebElement> usersList,String userName)
	{
		WebElement foundUser=null;
		for(WebElement user:usersList)
		{
			if(user.getText().equalsIgnoreCase(userName)) {
				
				System.out.println("user "+userName+" is Found");
				//break;
				foundUser=user;
				break;
			}
			//return foundUser;
			
		}
		return foundUser;
	}
	
	
	
	public UserDetailsPage clickOnAdd()
	{		
		//System.out.println("inside click on add method");
		getAddUserBtn().click();
		return new UserDetailsPage();
	}
	
	public boolean verifyUser(String userName)
	{
		boolean flag=true;
		if(getUser(getUsers(),userName)==null)
		{
			flag=false;
		}
		else
			flag=true;
//		if(!getUser(getUsers(),userName).isDisplayed()) {
//			flag=false;
//		}
//			
		return flag;
		
	}

//	public void deleteUser(String userName) throws InterruptedException
//	{
//		WebElement userTobeDeleted=getUser(getUsers(),userName);
//		getUserCheckBox(userTobeDeleted).click();
//		Thread.sleep(3000);
//		getDeleteBtn().click();
//		Thread.sleep(3000);
//		getDeleteOkBtn().click();
//		Thread.sleep(3000);
//	}
	
	public void clickOnDelete() {
		
		getDeleteBtn().click();
		
		
	}
	public boolean verifyDeleteBtnIsDisabled()
	{
		return getDeleteBtn().isEnabled();
	}
	public void selectUser(String userName) {
		WebElement userTobeDeleted=getUser(getUsers(),userName);
		getUserCheckBox(userTobeDeleted).click();
	}
	public void deleteUser()
	{
		clickOnDelete();
		getDeleteOkBtn().click();
	}
	public UserDetailsPage editUser(String userName)
	{
		WebElement userTobeEdited=getUser(getUsers(),userName);
		userTobeEdited.click();
		return new UserDetailsPage();
	}
	public boolean verifyEditUser(String userName) {
		
		boolean flag=false;
		UserDetailsPage userDetailsPage=editUser(userName);
		List<String> userDetails=userDetailsPage.verifyDetails();
		List<String> userDetailsFromConfig=new ArrayList<String>();
		userDetailsFromConfig.add(userProp.getProperty("userRoleEdit"));
		userDetailsFromConfig.add(userProp.getProperty("empNameEdit"));
		userDetailsFromConfig.add(userProp.getProperty("userNameEdit"));
		userDetailsFromConfig.add(userProp.getProperty("statusEdit"));
		
		if(userDetails.equals(userDetailsFromConfig))
			flag=true;
		return flag;
	}

}
