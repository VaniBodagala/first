package com.OrangeHRM.StepDefinitions;
import org.junit.Assert;
import com.OrangeHRM.util.BasePage;
import com.OrangeHRM.pages.DashboardPage;
import com.OrangeHRM.pages.LoginPage;

import io.cucumber.java.After;
import io.cucumber.java.Before;

//import com.OrangeHRM.Pages.DashboardPage;
//import com.OrangeHRM.Pages.LoginPage;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
public class OrangeHRMLoginSteps extends BasePage{

	LoginPage loginPage;
	DashboardPage dashboardPage;
	
	@Before("@First")
	public void launchUrl()
	{
		System.out.println("Before method Before initialization");
		BasePage.initialization();
		loginPage=new LoginPage();
	}
	
	@Given("user is on OrangeHRM Login page")
	public void user_is_on_OrangeHRM_Login_page() {
		System.out.println("Before initialization in loginsteps");
		//BasePage.initialization();
		//loginPage=new LoginPage();
		
		
		System.out.println("page title is: "+driver.getTitle());
	}

	
	@When("Login page is verified")
	public void login_page_is_verified() {
		Assert.assertTrue(loginPage.verifyLoginPanelIsDisplayed());
	}

	@Then("^user enters username and password and click on Login button$")
	public void user_enters_username_and_password_and_click_on_Login_button() {

		dashboardPage=loginPage.login(prop.getProperty("username"), prop.getProperty("password"));
		
	}

	@Then("user is navigated to dashboard page")
	public void user_is_navigated_to_dashboard_page() {
		
		
		Assert.assertEquals("Dashboard", dashboardPage.verifyDashboardPage());
		System.out.println("Page Title is: "+driver.getTitle());
		
		//BasePage.tearDown();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@Given("^User opens browser$")
    public void User_is_already_on_Loginpage() throws InterruptedException {
          System.out.println("user is already on login page");    
		//loginPage = new LoginPage();       
   }
	@Then("title should be OrangeHRM")
	public void title_should_be_OrangeHRM() {
		 
		String Title_name = loginPage.verifytitle();
		  
          Assert.assertEquals("OrangeHRM", Title_name);
          
		System.out.println("orangehrm title is" + Title_name); 
	}
	
	@Then("verify to see if OrangeHRMlogo is displayed or not")
	public void verify_to_see_if_OrangeHRMlogo_is_displayed_or_not() {
	   
		  boolean txt_displayed = loginPage.OrangehRmTxt();
		
		System.out.println("logoext is " + txt_displayed);
		
	}
	
	
	
	@Given("User should open browser")
	public void user_should_open_browser() throws InterruptedException {
		//BasePage.Initilization();
		//loginpage1 = new LoginPage();    
	}
	@Then("username should be Admin and password should be admin123 and click on Login button")
	public void username_should_be_Admin_and_password_should_be_admin123_and_click_on_Login_button() {
		dashboardPage =	loginPage.login(prop.getProperty("username"),prop.getProperty("password"));
	
	}
	@Then("user will be on Dashboard page")
	public void user_will_be_on_Dashboard_page() {
	    
		
		String dashboard_title = dashboardPage.verifyDashboardPage();
		
		Assert.assertEquals("Dashboard", dashboard_title);
			
	}
	@Then("click on logout link")
	public void click_on_logout_link() throws InterruptedException {
	
		loginPage.logout_link_click();   
	
		System.out.println("logout success");
	}
	@Then("verify footer text of OrangeHRm page")
	public void verify_footer_text_of_OrangeHRm_page() {

		boolean footer_txt = loginPage.verif_footertext();
		
		System.out.println("foottetx is" +  footer_txt );
		
		//driver.quit();
	}
	
	
	
	@Given("User should be loginpage")
	public void user_should_be_loginpage() {
	   System.out.println("Linked in scenario");
		//loginPage = new LoginPage(); 
		
	}
		
	@Then("check linkedin icon for hrmpage")
	public void check_linkedin_icon_for_hrmpage() throws InterruptedException {
	    loginPage.socialmedia_icon_li();
		
	    
	}
	
	//code for Fbook
	@Given("User should be loginpagehrm")
	public void user_should_be_loginpagehrm() {
		//loginPage = new LoginPage();   
		 System.out.println("Facebook scenario");
	}

	@Then("check Fbook icon for hrmpage")
	public void check_Fbook_icon_for_hrmpage() throws InterruptedException {
	   loginPage.socialmedia_icon_FB();
		
		
	}

	//code for twitter
	@Given("User has to be loginpage")
	public void user_has_to_be_loginpage() {
		//loginPage = new LoginPage();   
		 System.out.println("Twitter scenario");
	}

	@Then("test twitter link foor hrm page")
	public void test_twitter_link_foor_hrm_page() throws InterruptedException {
	     loginPage.socialmedia_icon_Twitt();
		
	}
	
	//code for youtube
	@Given("User must be loginpage")
	public void user_must_be_loginpage() {
		//loginPage = new LoginPage();  
		 System.out.println("youtube scenario");
	}
	
	@Then("test youtube link for hrm page")
	public void test_youtube_link_for_hrm_page() throws InterruptedException {
	     loginPage.socialmedia_icon_youtube();
		
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	@After
	public void teardown()
	{
		BasePage.tearDown();
	}
}



