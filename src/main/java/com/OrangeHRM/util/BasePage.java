package com.OrangeHRM.util;


import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import io.github.bonigarcia.wdm.WebDriverManager;

public class BasePage {

	public static Properties prop;
	public static Properties userProp;
	public static WebDriver driver;
	public static Properties jobProp;
	static String projectPath=System.getProperty("user.dir");
	
	public BasePage()
	{		
		try {
			prop=new Properties();
			FileInputStream fis=new FileInputStream(projectPath+"\\src\\main\\java\\com\\OrangeHRM\\config\\config.properties");
			prop.load(fis);

			userProp=new Properties();
			fis=new FileInputStream(projectPath+"/src/main/java/com/OrangeHRM/config/user.properties");
			userProp.load(fis);
			
//			FileInputStream jobTitlefile = new FileInputStream(projectPath+"/src/main/java/com/OrangeHRM/config/Job_Tab.properties");
//			jobProp.load(jobTitlefile);


		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public static void initialization()
	{
		String browserName=prop.getProperty("browser");
		if(browserName.equalsIgnoreCase("chrome")) {
			WebDriverManager.chromedriver().setup();
			driver=new ChromeDriver();
		}
		else if(browserName.equalsIgnoreCase("firefox"))
		{
			WebDriverManager.firefoxdriver().setup();
			driver=new FirefoxDriver();
		}
		else if (browserName.equalsIgnoreCase("edge")) {
			WebDriverManager.edgedriver().setup();
			driver = new EdgeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(TestUtil.PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
	}
	
	public static void tearDown()
	{
		driver.quit();
	}
	
	
	public WebElement getElement(By locator) {
		return driver.findElement(locator);
	}
	public List<WebElement> getElements(By locator)
	{
		return (List<WebElement>)driver.findElements(locator);
	}

public static void login() {
		driver.findElement(By.id("txtUsername")).sendKeys(prop.getProperty("username"));
		driver.findElement(By.id("txtPassword")).sendKeys(prop.getProperty("password"));
		driver.findElement(By.id("btnLogin")).click();
	}

}
