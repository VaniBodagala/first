package com.OrangeHRM.pages;

import java.util.Set;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.OrangeHRM.util.BasePage;

public class LoginPage extends BasePage{
	
	//madhavi locator's & webelements
	By OrangeHRm_txt = By.xpath("//*[@id=\'divLogo\']");  //getting location of that web element /identifying locater of webelement
	By username_txt=By.xpath("//input[@name='txtUsername']");
    By pass_word = By.xpath("//input[@id='txtPassword']");
    By Log_btn = By.xpath("//input[@value='LOGIN']");
   // By dashboard_heading = By.xpath("//h1[contains(text(),'Dashboard')]");
   
        //welcome dropdown & Logout link x-path's
    
    By welcome_drdw = By.xpath("//*[@id=\'welcome\']");
    By logout= By.xpath("//div[@id='welcome-menu']/ul/li/a[contains(text(),'Logout')]");
    
    By footer_text = By.xpath("//*[@id=\'footer\']/div[1]");
   // WebElement footertext;
    
    By socialicons_li = By.xpath("//div[@id='social-icons']//a[1]");
    By socialicons_Fb = By.xpath("//div[@id='social-icons']//a[2]");
    
    //end of madhavi locat& elements
    
	DashboardPage dashboardPage;
	private By txt_username=By.id("txtUsername");
	private By txt_password=By.id("txtPassword");
	private By btn_login=By.id("btnLogin");
	private By heading_loginPanel=By.id("logInPanelHeading");
	
	
	
	public WebElement getUsername()
	{
		return getElement(txt_username)	;
	}
	public WebElement getPassword()
	{
		return getElement(txt_password);
	}
	public WebElement getLoginBtn()
	{
		return getElement(btn_login);
	}
	public WebElement getLoginPanelHeading()
	{
		return getElement(heading_loginPanel);
	}
		
	

	public DashboardPage login(String username,String password)
	{
		getUsername().clear();
	    getUsername().sendKeys(username);
	    getPassword().clear();
		getPassword().sendKeys(password);
		getLoginBtn().click();
		return new DashboardPage();
	
	}
	
	//madhavi code begins
	public boolean verifyLoginPanelIsDisplayed()
	{
		return driver.findElement(heading_loginPanel).isDisplayed();
	}
	

	
	public String verifytitle() {
		
		return driver.getTitle();
		
		
	}	
		
	public boolean OrangehRmTxt() {
		
		boolean b2 = driver.findElement(OrangeHRm_txt).isDisplayed();
		return b2;
				
	}
	
//     public DashboardPage login(String un,String pwd) {
//		
//		driver.findElement(username_txt).sendKeys(un);
//		driver.findElement(pass_word).sendKeys(pwd);
//		driver.findElement(Log_btn).click();
//			
//		driver.manage().window().fullscreen();
//		
//		return new DashboardPage();
//	}
	
         
     public Void logout_link_click() throws InterruptedException {
 		
    	 driver.findElement(welcome_drdw).click();  //getting/finding element with the help of locater 
    	 
    	 Thread.sleep(2000);
    	 
    	 driver.findElement(logout).click();
		return null;
		
		
     }
    	
     public boolean verif_footertext() {
	   	 
    	 boolean footxt=  driver.findElement(footer_text).isDisplayed();
		return footxt;
    	 
     }	
     
     public void socialmedia_icon_li() throws InterruptedException {
    	 String currentHandle = driver.getWindowHandle();
 		
		 driver.findElement(By.xpath("//div[@id='social-icons']//a[1]")).click();
		 
		 driver.manage().window().maximize();
		 
		 driver.manage().deleteAllCookies();
		 
		 System.out.println(currentHandle);
		 
		 Set handles = driver.getWindowHandles();
		 
         System.out.println(handles);
 
        // String currentHandle= driver.getWindowHandle();
         
         // Pass a window handle to the other window
 
         for (String handle1 : driver.getWindowHandles()) {
    
        	 if(!handle1.equalsIgnoreCase(currentHandle))
	         {
	             //switching to the opened tab
	          
         	System.out.println(handle1);
 
         	driver.switchTo().window(handle1);
         	
         	Thread.sleep(4000);
         	}
         } 
         
         driver.findElement(By.linkText("Sign in")).click();
		 Thread.sleep(2000);
		 
         driver.findElement(By.xpath("//input[@id='username']")).sendKeys("mrgunnala@hotmail.com");
		 driver.findElement(By.xpath("//input[@id='password']")).sendKeys("Gade247399");
		 driver.findElement(By.xpath("//button[@type='submit']")).click();
		 Thread.sleep(2000);
		 
		 //driver.close();
		
		 driver.switchTo().window(currentHandle);//will swithch to parent window
 	}
    
     public void socialmedia_icon_FB() throws InterruptedException {
    	     
    	 String parentHandle1 = driver.getWindowHandle();
  		
		 driver.findElement(By.xpath("//div[@id='social-icons']//a[2]")).click();
		 
		 driver.manage().window().maximize();
		 
		 driver.manage().deleteAllCookies();
		 
		 System.out.println(parentHandle1);
		 
		 Set handles = driver.getWindowHandles();
		 
         System.out.println(handles);
 
        // String currentHandle= driver.getWindowHandle();
         
         // Pass a window handle to the other window
 
         for (String handle1 : driver.getWindowHandles()) {  //
    
        	 if(!handle1.equalsIgnoreCase(parentHandle1))
	         {
	             //switching to the opened tab
	          
         	System.out.println(handle1);
 
         	driver.switchTo().window(handle1);
         	
         	Thread.sleep(4000);
         	
         	}
         }         
			 
		 driver.switchTo().defaultContent();
		
     
     }  
     
     public void socialmedia_icon_Twitt() throws InterruptedException {
	     
    	 String parentHandle2 = driver.getWindowHandle();
  		
		 driver.findElement(By.xpath("//div[@id='social-icons']//a[3]")).click();
		 
		 driver.manage().window().maximize();
		 
		 driver.manage().deleteAllCookies();
		 
		 System.out.println(parentHandle2);
		 
		 Set handles = driver.getWindowHandles();
		 
         System.out.println(handles);
 
        // String currentHandle= driver.getWindowHandle();
         
         // Pass a window handle to the other window
 
         for (String handle1 : driver.getWindowHandles()) {  //
    
        	 if(!handle1.equalsIgnoreCase(parentHandle2)){
	             //switching to the opened tab
	          
         	System.out.println(handle1);
 
         	driver.switchTo().window(handle1);
         	
         	Thread.sleep(4000);
         	
         	}
         }         
				 
		 driver.switchTo().defaultContent();
		
     
     } 
    
 public void socialmedia_icon_youtube() throws InterruptedException {
	     
    	 String parentHandle3 = driver.getWindowHandle();
  		
		 driver.findElement(By.xpath("//div[@id='social-icons']//a[4]")).click();
		 
		 driver.manage().window().maximize();
		 
		 driver.manage().deleteAllCookies();
		 
		 System.out.println(parentHandle3);
		 
		 Set handles = driver.getWindowHandles();
		 
         System.out.println(handles);
 
        // String currentHandle= driver.getWindowHandle();
         
         // Pass a window handle to the other window
 
         for (String handle1 : driver.getWindowHandles()) {  //
    
        	 if(!handle1.equalsIgnoreCase(parentHandle3)){
	             //switching to the opened tab
	          
         	System.out.println(handle1);
 
         	driver.switchTo().window(handle1);
         	
         	Thread.sleep(4000);
         	
         	}
         }         
				 
		 driver.switchTo().defaultContent();
		
     
     } 
     
     
     
     
}
     


	

