package com.OrangeHRM.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;

//import com.OrangeHRM.util.BasePage;
import com.OrangeHRM.util.BasePage;
public class UserDetailsPage extends BasePage {
	
	private By select_role=By.id("systemUser_userType");
	private By txt_empName=By.id("systemUser_employeeName_empName");
	private By txt_username=By.id("systemUser_userName");
	private By select_status=By.id("systemUser_status");
	private By txt_password=By.id("systemUser_password");
	private By txt_cfm_password=By.id("systemUser_confirmPassword");
	private By btn_save=By.id("btnSave");
	
	public WebElement getUserRole()
	{
		return getElement(select_role);
	}
	public WebElement getEmpName()
	{
		return getElement(txt_empName);
	}
	public WebElement getUsername()
	{
		return getElement(txt_username);
	}
	public WebElement getStatus()
	{
		return getElement(select_status);
	}
	public WebElement getPassword()
	{
		return getElement(txt_password);
	}
	public WebElement getCfmPassword()
	{
		return getElement(txt_cfm_password);
	}
	public WebElement getSaveBtn()
	{
		return getElement(btn_save);
	}
	
	
	public void enter_new_user_details(String userRole,String empName,String username,String status,String password) throws InterruptedException
	{
		Select selectRole=new Select(getUserRole());
		selectRole.selectByVisibleText(userRole);
		
//		Double d=(Math.random())*30;
//		System.out.println(d);
//		Integer i=(int)Math.round(d);
		getEmpName().clear();
		getEmpName().sendKeys(empName);
		getUsername().clear();
		getUsername().sendKeys(username);
		
		Select selectStatus=new Select(getStatus());
		selectStatus.selectByVisibleText(status);
		
		getPassword().sendKeys(password);
		getCfmPassword().sendKeys(password);
		Thread.sleep(2000);
		getSaveBtn().click();
		Thread.sleep(3000);
	}
	
	public void enter_edit_user_details(String userRole,String empName,String username,String status) throws InterruptedException
	{
		getSaveBtn().click();
		Select selectRole=new Select(getUserRole());
		selectRole.selectByVisibleText(userRole);
		Thread.sleep(3000);
		getEmpName().clear();
		getEmpName().sendKeys(empName);
		getUsername().clear();
		getUsername().sendKeys(username);
		
		Select selectStatus=new Select(getStatus());
		selectStatus.selectByVisibleText(status);
		
		Thread.sleep(3000);
		getSaveBtn().click();
		//return newUserPage();
	}
	

	public boolean verifyAllFiledsAreDisabled() {
		boolean flag=false;
		if(!getUserRole().isEnabled()&& !getEmpName().isEnabled()&& !getUsername().isEnabled()&& !getPassword().isEnabled())
			flag=true;
		return flag;
	}
	public List<String> verifyDetails()
	{
		List <String> userDetails=new ArrayList<String>();
		Select selectRole=new Select(getUserRole());
		userDetails.add(selectRole.getFirstSelectedOption().getText());
		System.out.println(selectRole.getFirstSelectedOption().getText());
			userDetails.add(getEmpName().getAttribute("value"));
			userDetails.add(getUsername().getAttribute("value"));
			
			Select selectStatus=new Select(getStatus());
			
			userDetails.add(selectStatus.getFirstSelectedOption().getText());
			System.out.println("list of user details are: "+userDetails);
			return userDetails;
	}

}
