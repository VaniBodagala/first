package com.OrangeHRM.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;

import com.OrangeHRM.util.BasePage;

public class DashboardPage extends BasePage {

	UserPage userPage;
	private By heading_dashboard=By.xpath("//h1[contains(text(),'Dashboard')]");
	private By menu_admin=By.xpath("//b[contains(text(),'Admin')]");
///////

	By Dashboard_Label = By.xpath("//div[@class='head']//h1");
	By Admin_btn = By.xpath("//b[contains(text(),'Admin')]");

	
	public WebElement getDashboardHeading()
	{
		return getElement(heading_dashboard);
	}
	public WebElement getAdminMenu()
	{
		return getElement(menu_admin);
	}
	
	
	public AdminPage clickOnAdmin()
	{
		getAdminMenu().click();
		return new AdminPage();
	}
	
	public String verifyDashboardPage()
	{
		System.out.println("inside verify dashboard page method");
		return getDashboardHeading().getText();
	}
//////////

public String getDashboardTitle() {
		return driver.findElement(Dashboard_Label).getText();
	}
	
	public AdminPage clickAdmin_btn() {
		
		driver.findElement(Admin_btn).click();
		return new AdminPage();
	}
//////madhavi code
	/*By dashboard_heading = By.xpath("//h1[contains(text(),'Dashboard')]");
	
	  public String verif_dashboard_page() {
	   	 
	    	 return driver.findElement(dashboard_heading).getText();
	    	 
	     }*/


}
