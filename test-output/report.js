$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("file:src/test/resources/features/OrangeHRMLogin.feature");
formatter.feature({
  "name": "feature to verify OrangeHRM login with valid credentials",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "verify OrangeHRM admin login functionality",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@First"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user is on OrangeHRM Login page",
  "keyword": "Given "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRMLoginSteps.user_is_on_OrangeHRM_Login_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "Login page is verified",
  "keyword": "When "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRMLoginSteps.login_page_is_verified()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user enters username and password and click on Login button",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRMLoginSteps.user_enters_username_and_password_and_click_on_Login_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user is navigated to dashboard page",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRMLoginSteps.user_is_navigated_to_dashboard_page()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.uri("file:src/test/resources/features/OrangeHRMUserManagement.feature");
formatter.feature({
  "name": "feature to verify user management",
  "description": "",
  "keyword": "Feature"
});
formatter.scenario({
  "name": "verify OrangeHRM admin add user functionality",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@User"
    },
    {
      "name": "@Second"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "user logged in as admin",
  "keyword": "Given "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.user_logged_in_as_admin()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user click on admin and hover usermanagement and click on users",
  "keyword": "When "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.hover_admin_and_usermanagement_and_click_on_users()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Add user",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.click_on_Add_user()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "enter userRole, employeeName, username and status and click on Save button",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.enter_new_user_details()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "User should be added in the users list",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.user_should_be_added_in_the_users_list()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "verify OrangeHRM edit user functionality",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@User"
    },
    {
      "name": "@Second"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "admin should be logged in and go to users page",
  "keyword": "Given "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.admin_should_be_logged_in_and_go_to_users_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "admin selects user to be edited",
  "keyword": "When "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.admin_selects_user_to_be_edited()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "all the fields should be disabled",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.all_the_fields_should_be_disabled()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "click on Edit button and edit the fields and click on Save button",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.click_on_Edit_button_and_edit_the_fields_and_click_on_Save_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "all the changes must be reflected when you click on the user again",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.all_the_changes_must_be_reflected_when_you_click_on_the_user_again()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "verify delete button is disabled",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@User"
    },
    {
      "name": "@Second"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "admin is already logged in and navigated to users page",
  "keyword": "Given "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.admin_is_already_logged_in_and_navigated_to_users_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "user clicks on delete button",
  "keyword": "When "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.user_clicks_on_delete_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "nothing should happen delete button must be disabled",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.nothing_should_happen_delete_button_must_be_disabled()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.scenario({
  "name": "verify OrangeHRM delete user functionality",
  "description": "",
  "keyword": "Scenario",
  "tags": [
    {
      "name": "@User"
    },
    {
      "name": "@Second"
    }
  ]
});
formatter.before({
  "status": "passed"
});
formatter.step({
  "name": "admin logs in and go to admin page",
  "keyword": "Given "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.admin_logs_in_and_go_to_admin_page()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "admin selects user to be deleted from the list of users",
  "keyword": "When "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.admin_selects_user_to_be_deleted_from_the_list_of_users()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "delete button should be enabled and click on delete button",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.delete_button_should_be_enabled_and_click_on_delete_button()"
});
formatter.result({
  "status": "passed"
});
formatter.step({
  "name": "selected user should be deleted from the users list",
  "keyword": "Then "
});
formatter.match({
  "location": "com.OrangeHRM.StepDefinitions.OrangeHRM_UserManagement_Steps.selected_user_should_be_deleted_from_the_users_list()"
});
formatter.result({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
formatter.after({
  "status": "passed"
});
});